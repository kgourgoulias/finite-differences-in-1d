% This script produces the different graphs used in this exercise.  
% There are two free variables that can be changed, until and NoOfTests.

% until : if until = 4, then the graphs will be for the nodes 
% m = 10 * [2^(1 : 3)]. If until is any other number n, then the graphs will
% be for the nodes m = 10 * [2^(1:n-1)]

% NoOfTests : This variable changes the number of times that the script will
% run the two functions, findiff and fPoissonSolve. The reason for that
% is to make sure that the programs that run together with matlab won't 
% affect the CPU time measurements.

%This script was written as part of a H/W for the Math652 course

%Konstantinos Gourgoulias
%gourgoul@math.umass.edu

close all;

until = 7;
NoOfTests = 7;

 m = 10*2.^[1:until-1];
until = length(m);

%===================================================
%=============== Computation =======================
L2bSlash = zeros(1,until);
L2bSine  = zeros(1,until);

cpuTimeSlash = L2bSlash;
cpuTimeSine  = L2bSlash;
%m = 10*[1,2.^(1:until-1)];

cpuTimeTemp1  = zeros(1,NoOfTests); 
cpuTimeTemp2  = cpuTimeTemp1;

for i = 1:until
    for j = 1 : NoOfTests
%         [L2bSlash(i), cpuTimeTemp1(j)] = findiff(m(i));
        [L2bSine(i), cpuTimeTemp2(j)] = fPoissonSolve(m(i));
    end
%     cpuTimeSlash(i) = mean(cpuTimeTemp1);
    cpuTimeSine(i) = mean(cpuTimeTemp2);
end

%==================== Plots ==============================
% % figure;
% plot(log(m), log(L2bSlash),'r-O',log(m),log(L2bSine),'b*');
% title('Number of nodes vs L2 error');
% xlabel('log(m)');
% ylabel('log(L2 error)');
% legend('Slash', 'DST','Location','Best');
% figure;
% plot(m,cpuTimeSlash,'r-*',m,cpuTimeSine,'b-*');
% title('Number of nodes vs CPU time');
% xlabel('m');
% ylabel('cpuTime (sec)');
% legend('Slash', 'DST','Location','Best');


% figure;
% p = log2(L2bSlash(1:until-1)./L2bSlash(2:until));
% plot(m(1:until-1),p,'b*-');
% title('Approximate order of the error, using /');


figure;
p = log2(L2bSine(1:until-1)./L2bSine(2:until));
plot(m(1:until-1),p,'r*-');
title('Approximate order of the error, using DST');

x = m;
y = x.*log(x)/max(x.*log(x)).*max(cpuTimeSine)+min(cpuTimeSine);

figure;
plot(m,y,'r-*',m,cpuTimeSine,'b-*');
title('Number of nodes vs CPU time');
xlabel('m');
ylabel('cpuTime (sec)');
legend('m*log(m)', 'DST','Location','Best');


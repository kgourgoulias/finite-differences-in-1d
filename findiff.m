function [L2error, cpuTime, Uappr] = findiff(m,f,a,b)
% FINDIFF solves the poisson equation in 1D 

% findiff solves the PDE : 
%               Uxx = f(x), x in [0,1], 
% with boundary conditions u(0) = a, u(1) = b
% m : Number of nodes to use. 
% f : right hand side of the equation
% 
% This function also calculates the L2 error for the case where the
% solution of the equation is u(x) = e^(sin(10*pi*x)).
% For a different solution, just change the variable Uexact. 

% This function supports standard arguments. Calling it without arguments
% will run it for m = 60 and f the one in the H/W. Calling it with only the
% m argument will run it with f the one in the H/W. 

% This function was written as part of a H/W for the Math652 course.

% Konstantinos Gourgoulias
% gourgoul@math.umass.edu


%==================This code is about the different arguments ============

switch nargin
    case 0 
        m =100;
        b = 2*pi; 
        a = 0;
        
        h = (b-a)/(m+1);
        x = ((1 : (m+2)) - 1)*h;
        xtrunc = x(2:m+1);
       %F = 100*pi^2*exp(sin(10*pi*xtrunc)).*...
       %(cos(10*pi*xtrunc).^2-sin(10*pi*xtrunc));
        F = sin(xtrunc); 
        c = 1;
        d = 1 - sin(2*pi);
    case 1
        
         b = 2*pi; 
        a = 0;
        
        h = (b-a)/(m+1);
        x = ((1 : (m+2)) - 1)*h;
        xtrunc = x(2:m+1);

        F = sin(xtrunc); 
        c = 1;
        d = 1 - sin(2*pi);
%         x = ((1 : (m+2)) - 1)*1/(m+1);
%         xtrunc = x(2:m+1);
%         F = 100*pi^2*exp(sin(10*pi*xtrunc)).*...
%     (cos(10*pi*xtrunc).^2-sin(10*pi*xtrunc));
%         a = 1;
%         b = 1;
    case 2
        x = ((1 : (m+2)) - 1)*1/(m+1);
        xtrunc = x(2:m+1);
        F = f(xtrunc);
        a = 1;
        b = 1;
    case 3
        b = input(['You have to input the'...
            'second boundary condition as well : ']);
        x = ((1 : (m+2)) - 1)*1/(m+1);
        F = f(x(2:m+1));
    case 4
        x = ((1 : (m+2)) - 1)*1/(m+1);
        F = f(x(2:m+1));
end

%============ Actual code starts here =======================


% Calculating the value of the exact solution at the different
% m+2 nodes.
 Uexact = 1 - sin(x);
%Uexact = exp(sin(10*pi*x));


% Building the sparse finite differences matrix


e = ones(m,1);
A = spdiags([e,-2*e,e],[-1,0,1],m,m)./h^2;



%Solving the system and "attaching" the values we know from the boundary
%conditions. 

F(1) = F(1) - c/h^2;
F(end) = F(end) - d/h^2;

tic;
Uappr = [c,(A\F.').',d];

cpuTime = toc;

L2error = (h*sum(abs(Uexact - Uappr).^2))^(1/2);



 plot(x,Uappr,'b',x,Uexact,'r');
 legend('Uappr','Uexact');
 title(['m = ',num2str(m)]);
shg;

end
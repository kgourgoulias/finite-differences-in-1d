% This script produces a comparison between the actual computational time
% of the discrete sine transform and the theoritical value of O(nlog(n))

% In order to do that, we produce a vector m with the numbers between 10
% and until variable, with a step 10. Then we calculate the solution of
% problem using the DST and time it for the same number of nodes NoOfTests
% times. We calculate m*log(m) and map the values to the interval that we
% have the different execution times and produce a graph to compare them.


% NoOfTests : This variable changes the number of times that the script will
% run the two functions, findiff and fPoissonSolve. The reason for that
% is to make sure that the programs that run together with matlab won't 
% affect the CPU time measurements.

%This script was written as part of a H/W for the Math652 course

%Konstantinos Gourgoulias
%gourgoul@math.umass.edu

clear all;

until = 600;
NoOfTests = 7;

 m = 10*(1:5:until);
until = length(m);

%===================================================
%=============== Computation =======================
L2bSine  = zeros(1,until);


cpuTimeSine  = L2bSine;

cpuTimeTemp2  = zeros(1,NoOfTests); 

for i = 1:until
    for j = 1 : NoOfTests
        [L2bSine(i), cpuTimeTemp2(j)] = fPoissonSolve(m(i));
    end
    cpuTimeSine(i) = mean(cpuTimeTemp2);
end

%==================== Plots ==============================

x = m;
y = x.*log(x)/max(x.*log(x)).*max(cpuTimeSine)+min(cpuTimeSine);

figure;
plot(m,y,'r-*',m,cpuTimeSine,'b-*');
title('Number of nodes vs CPU time');
xlabel('m');
ylabel('cpuTime (sec)');
legend('m*log(m)', 'DST','Location','Best');

%================ Clean up ===============================
clear cpuTimeTemp1 cpuTimeTemp2 NoOfTests;
function [L2error, cpuTime] = fPoissonSolve(m,f,a,b)
%Fast Poisson Solver : Solves the poisson equation in 1D


% fPoissonSolve solves the PDE : 
%               Uxx = f(x), x in [0,1], 
% with boundary conditions u(0) = a, u(1) = b
% m : Number of nodes to use. 
% f : right hand side of the equation
% by using the DST (Discrete Sine transform) and it's inverse. 

% This function also calculates the L2 error for the case where the
% solution of the equation is u(x) = e^(sin(10*pi*x)).
% For a different solution, just change the variable Uexact. 

% This function supports standard arguments. Calling it without arguments
% will run it for m = 60 and f the one in the H/W. Calling it with only the
% m argument will run it with f the one in the H/W. 

% This function was written as part of a H/W for the Math652 course.

% Konstantinos Gourgoulias
% gourgoul@math.umass.edu

%==================This code is about the different arguments ============
switch nargin
    case 0 
        m = 80;
        a = 0;
        b = 1;
        h = (b-a)/(m+1);
        x = ((1 : (m+2)) - 1)*h + a;
        xtrunc = x(2:m+1);
        F = 100*pi^2*exp(sin(10*pi*xtrunc)).*...
        (cos(10*pi*xtrunc).^2-sin(10*pi*xtrunc));
        c = 1; 
        d = 1;
    case 1
        
         a = 0;
        b = 1;
        h = (b-a)/(m+1);
        x = ((1 : (m+2)) - 1)*h + a;
        xtrunc = x(2:m+1);
         F = 100*pi^2*exp(sin(10*pi*xtrunc)).*...
        (cos(10*pi*xtrunc).^2-sin(10*pi*xtrunc));
       
        c = 1; 
        d = 1 - sin(b);
    case 2
        x = ((1 : (m+2)) - 1)*1/(m+1);
        xtrunc = x(2:m+1);
        F = f(xtrunc);
        a = 1;
        b = 1;
    case 3
        b = input(['You have to input the'...
            'second boundary condition as well : ']);
        x = ((1 : (m+2)) - 1)*1/(m+1);
        F = f(x(2:m+1));
    case 4
        x = ((1 : (m+2)) - 1)*1/(m+1);
        F = f(x(2:m+1));
end
%============ Actual code starts here =======================

 %Calculating the exact solution on the nodes
 Uexact = exp(sin(10*pi*x));

 
 

 
 % Changing F, the right hand vector of the system, according to the
 % boundary conditions
 F(1) = F(1) - c/h^2;
 F(end) = F(end) - d/h^2;
 
 tic;
 % Calculating the inverse sine transform of F, S^{-1}F, if S is the matrix
 % with the eigenvector at it's columns.
 as = 2/(length(F)+1)*DST1(F(:));
 
 % Calculating the eigenvalues and then doing S^{-1}*F*L^{-1}, where L is
 % the diagonal matrix of the eigenvalues
 lambda = 2/h^2 * (cos((1:length(F))*pi/(m+1)) - 1);
 
 as = (as.')./lambda;
 
 cpuTime = toc;

 
 % Using the DST on the previous result, thus acquiring the solution of the
 % system. a and b are the known values at the boundary. 
 Uappr = ([c;DST1(as(:));d]).';


L2error = (h*sum(abs(Uexact - Uappr).^2))^(1/2);
% 
%  
%  plot(x,Uappr,'b',x,Uexact,'r');
%  legend('Uappr','Uexact');
%  title(['m = ',num2str(m)]);
% shg;


end






  function [y] = DST1(u)

%  u = b;

[m, K ] =size(u); m = m+1;

y = zeros(2*m, K);

y(2:m, :) = u;

y(m+2:2*m,:)  = -u(m-1:-1:1,:);

y = ifft(y);

y = imag( y(2:m, :) )*(m);

  end

  
  function B = myPermute(A)
%Used in the fast poisson algorithm
    [~,IX] = sort(diag(A), 'descend');
     B = A(IX,:);
end